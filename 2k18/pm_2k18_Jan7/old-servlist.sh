#!/bin/bash
#set -x
j=1
#cat list;

#cat list | while IFS= read -r i

for i in $(cat list); 
do
	echo -ne "$j\t"
#	echo -e "\t" 
	let j++
	echo -ne "$(egrep "Hostname" extracted/$i | cut -d':' -f2) \t\t"
#	echo -e "\t"
	echo -ne "$(egrep "IP addresses" extracted/$i | awk '{print $4}') \t"
	
	egrep "OS Release" extracted/$i | cut -d':' -f2 > /tmp/VAR_OS_REL
	sed -i 's/Red Hat Enterprise Linux Server release/RHEL/g' /tmp/VAR_OS_REL
	sed -i 's/CentOS release/Centos/g' /tmp/VAR_OS_REL
	echo -ne "$(cat /tmp/VAR_OS_REL) \n"
#	echo -e "\n";
done
