#!/bin/bash
>/tmp/VARIABLE_ONE
>/tmp/VARIABLE_TWO

  for i in $(cat list); do
	grep -e "Hostname" extracted/$i | awk '{print $3}' | tr '\r\n' ':' >> /tmp/VARIABLE_ONE
	grep -e "IP's" extracted/$i | awk '{print $3}' | tr '\r\n' ':' >> /tmp/VARIABLE_ONE
	grep -e "OS Release" extracted/$i | awk '{print $4,$5,$6,$7,$8,$9,$10}' | tr -d " " > /tmp/VARIABLE_TWO
	sed -e "s/RedHatEnterpriseLinuxServerrelease/RHEL /g" /tmp/VARIABLE_TWO >> /tmp/VARIABLE_ONE	
  done
awk '{printf("%02d %s\n", NR, $0)}' /tmp/VARIABLE_ONE | column -t -s ":"

