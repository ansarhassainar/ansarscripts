#!/bin/bash

# Periodic Maitenance Report 2018
# Updated disk function on 28th Jan
# Corrected uptime function on 15thfeb
# Added servers list function
# Replaced memory function and added cpu function -may1

#edited june1
######

#Note: 
#Create a folder extracted in same location of this script
#Move all reports from servers to this folder(filename of each server should be hostname of the server)
#Run this script

######
>/tmp/VARIABLE_NOTES

DATE=$(date +%d%b%Y)
PM_REPORT=/tmp/$DATE-pm-report
touch $PM_REPORT
touch /tmp/VARIABLE_OKAY
touch /tmp/VARIABLE_NTOKAY
touch /tmp/VARIABLE_OKAY_OUT

RED='\033[0;31m'
GREEN='\033[0;32m'
U_L='\e[4m'
NC='\033[0m'
#echo -e "\e[4m Hello \e[0m"
#"\e[4m Hello \e[om"


servers_list()
{
>/tmp/VARIABLE_OKAY
>/tmp/VARIABLE_NTOKAY
>/tmp/VARIABLE_OKAY_OUT
echo -e "Performed Health Check for below servers as a part of Periodic Maintenance Visit.

List of Servers:"
  for i in $(cat list); do
	grep -e "Hostname" extracted/$i | awk '{print $3}' | tr '\r\n' ':' >> /tmp/VARIABLE_OKAY
	grep -e "IP's" extracted/$i | awk '{print $3}' | tr '\r\n' ':' >> /tmp/VARIABLE_OKAY
	grep -e "OS Release" extracted/$i | cut -d':' -f2 | tr -d " " > /tmp/VARIABLE_NTOKAY
	sed -e "s/RedHatEnterpriseLinuxServerrelease/RHEL /g" /tmp/VARIABLE_NTOKAY >> /tmp/VARIABLE_OKAY	
	sed -i "s/RedHatEnterpriseLinuxAtomicHostrelease/RHEL Atomic /g" /tmp/VARIABLE_OKAY
  done
awk '{printf("%02d   %s\n", NR, $0)}' /tmp/VARIABLE_OKAY >> /tmp/VARIABLE_OKAY_OUT
sed -i "1iNo   Host Name:IP Address:OS Version" /tmp/VARIABLE_OKAY_OUT
column -t -s ":" /tmp/VARIABLE_OKAY_OUT

}

rhn_refine()
{
>/tmp/VARIABLE_OKAY
>/tmp/VARIABLE_NTOKAY

for i in $(cat list); do
        if [ $(grep -c "System has a Valid RedHat registration"  extracted/$i) -eq 1 ]; then
           echo $i >> /tmp/VARIABLE_OKAY
#        fi
        elif [ $(grep -c "Not Registered with RedHat !!!" extracted/$i) -eq 1 ]; then
           echo $i >> /tmp/VARIABLE_NTOKAY
	else
	  echo $i - Check not done >> /tmp/VARIABLE_NTOKAY
        fi
done
[ -s /tmp/VARIABLE_NTOKAY ] && echo -e "1. Following servers noted as not registered with Red Hat Subscription Management, therefore not receiving any security updates. KuwaitNET highly recommends registering all production servers with Red Hat Subscription Management. \n" \
                         && awk '{printf("%02d - %s\n", NR, $0)}' /tmp/VARIABLE_NTOKAY | paste -d: - - | column -t -s ":" \
                         || echo -e "1. All servers registered with Red Hat subscription management and receiving security updates to be applied."
#For detailed Report
#[ -s /tmp/VARIABLE_OKAY ] && echo -e ""$U_L"Systems Registered with RedHat: \n$NC$GREEN$(cat /tmp/VARIABLE_OKAY)$NC"
#[ -s /tmp/VARIABLE_NTOKAY ] && echo -e ""$U_L"Systems Not Registered: \n$NC$RED$(cat /tmp/VARIABLE_NTOKAY)$NC" || echo -e "$GREEN All Systems Registered with RedHat $NC"
}

disk_refine()
{
>/tmp/VARIABLE_OKAY
>/tmp/VARIABLE_NTOKAY
for i in $(cat list); do 
	if [ $(grep -c "Disk usage is normal"  extracted/$i) -eq 1 ]; then
	   echo $i >> /tmp/VARIABLE_OKAY
	fi
	if [ $(grep -c "Disk usage Critical" extracted/$i) -ge 1 ]; then
	   echo "$i" | tr '\r\n' ':' >> /tmp/VARIABLE_NTOKAY
	   grep -e "Disk usage Critical" extracted/$i | cut -d':' -f2,3 | cut -d':' -f2  >> /tmp/VARIABLE_NTOKAY
	fi
done

[ -s /tmp/VARIABLE_NTOKAY ] && echo -e "\n2. Disk Usage noted as critical in following servers:\n" \
			    && sed -i "1iHost Name:Disk Usage Details" /tmp/VARIABLE_NTOKAY \
			    && column -t -s ":" /tmp/VARIABLE_NTOKAY \
      			    || echo -e "\n2. Disk usage noted as normal in all servers and not exceeding threshold limit."
#For detailed Report
#echo -e "\e[4m Hello \e[0m"
#[ -s /tmp/VARIABLE_OKAY ] && echo -e ""$U_L"Disk Usage normal in following servers:\n$NC$GREEN$(cat /tmp/VARIABLE_OKAY)$NC"
#[ -s /tmp/VARIABLE_NTOKAY ] && echo -e ""$U_L"Disk Usage critical in following servers:\n$NC$RED$(cat /tmp/VARIABLE_NTOKAY)$NC" || echo -e "$GREEN Disk usage is normal in all servers $NC"
}

ntp_refine()
{

>/tmp/VARIABLE_OKAY
>/tmp/VARIABLE_NTOKAY
>/tmp/VARIABLE_OKAY_OUT
# STR="eeeeeee"; printf 'ddd %22s%-11s dddd \n'

for i in $(cat list); do
        NTP_YES=$(cat extracted/$i | grep -A15 "NTP Service Info" | grep -c "Ntp Configured in the server" )
        NTP_SERVICE=$(cat extracted/$i | grep -A15 "NTP Service Info" | grep  "Ntp Configured in the server" | awk '{print $1}' )

        if [ $NTP_YES == 1 ]; then

                if [ $NTP_SERVICE == CHRONYD ]; then

                IP_NTP_SERVER=$(cat extracted/$i | grep -A15 "NTP Service Info"  | grep "Reference ID" | awk '{print $5}' | cut -d'(' -f2 | cut -d')' -f1)
                echo -e "$i:$IP_NTP_SERVER" >> /tmp/VARIABLE_OKAY

                elif [ $NTP_SERVICE == NTPD ]; then

                IP_NTP_SERVER=$(cat extracted/$i | grep -A15 "NTP Service Info" | grep -i "synchronised to NTP server" | awk '{print $5}' | cut -d'(' -f2 | cut -d ')' -f1)
                echo -e "$i:$IP_NTP_SERVER" >> /tmp/VARIABLE_OKAY
                fi
                fi


        NTP_NO=$(cat extracted/$i | grep -A15 "NTP Service Info" | grep -c "Ntp service not configured in the server")
        if [ $NTP_NO == 1  ]; then
        echo $i >> /tmp/VARIABLE_NTOKAY;
        fi
done

cat /tmp/VARIABLE_OKAY >> /tmp/VARIABLE_NOTES

#[ -s /tmp/VARIABLE_OKAY ] && awk '{printf("%02d %s\n", NR, $0)}' /tmp/VARIABLE_OKAY >> /tmp/VARIABLE_OKAY_OUT \
#			  && sed -i "1iHostname:NTP Server IP" /tmp/VARIABLE_OKAY_OUT \
#			  && sed -i "2i--------:-------------" /tmp/VARIABLE_OKAY_OUT \
#			  && echo -e "Ntp configured in : \n$(column -t -s ":" /tmp/VARIABLE_OKAY_OUT)"
[ -s /tmp/VARIABLE_NTOKAY ] && echo -e "\n3. Ntp not configured in following servers :\n \n$(awk '{printf("%02d - %s\n", NR, $0)}' /tmp/VARIABLE_NTOK
AY | paste -d: - - | column -t -s ":" )"  \
 			    || echo -e "\n3. NTP Configured in the server and synchronizing with NTP server $(cat /tmp/VARIABLE_OKAY | cut -d':' -f2 | uniq)."
}


memory_refine()
{
>/tmp/VARIABLE_OKAY
>/tmp/VARIABLE_NTOKAY
>/tmp/VARIABLE_NTOKAY_OUT

for i in $(cat list); do

vl=$(grep "Memory usage" extracted/$i  | awk '{print $4}')

if [[ $(bc -l <<< "$vl < 90") -eq 1 ]]; then
	#memory usage normal list
	 echo "$i" >> /tmp/VARIABLE_OKAY
  else 
	 echo "$i:$vl%"  >> /tmp/VARIABLE_NTOKAY
fi
done

[ -s /tmp/VARIABLE_NTOKAY ] && echo -e "\n5. Memory usage in following servers noted as above 90 percentage : \n$(column -t -s ":" /tmp/VARIABLE_NTOKAY >> /tmp/VARIABLE_NTOKAY_OUT \
			    && awk '{printf("%02d %s\n", NR, $0)}' /tmp/VARIABLE_NTOKAY_OUT )" \
			    || echo -e "\n5. Memory usage of all servers noted as normal"
}

cpu_refine()
{
>/tmp/VARIABLE_OKAY
>/tmp/VARIABLE_NTOKAY
>/tmp/VARIABLE_NTOKAY_OUT

for i in $(cat list); do

vl=$(grep "CPU Usage" extracted/$i  | awk '{print $4}')

if [[ $(bc -l <<< "$vl < 90") -eq 1 ]]; then
        #CPU usage normal list. ie. usage below 90%
         echo "$i" >> /tmp/VARIABLE_OKAY
  else
        #CPU usage greater than 90%
         echo "$i:$vl%"  >> /tmp/VARIABLE_NTOKAY
fi
done

[ -s /tmp/VARIABLE_NTOKAY ] && echo -e "\n6. CPU usage in following servers noted as above 90 percentage : \n$(column -t -s ":" /tmp/VARIABLE_NTOKAY >> /tmp/VARIABLE_NTOKAY_OUT \
                            && awk '{printf("%02d %s\n", NR, $0)}' /tmp/VARIABLE_NTOKAY_OUT )" \
                            || echo -e "\n6. CPU usage of all servers noted as normal"

}

load_refine()
{
>/tmp/VARIABLE_OKAY
for i in $(cat list); do
        CPU_NUM=$(egrep "Number of CPUs" extracted/$i | awk '{print $5}')
	LOAD_VALUE=$(egrep "Load Average" extracted/$i | awk '{print $4}')
        L1=$(egrep "load average" extracted/$i |grep -v top | awk '{print $8,$9,$10,$11,$12}' | cut -d':' -f2| cut -d',' -f1 )
        L2=$(egrep "load average" extracted/$i |grep -v top | awk '{print $8,$9,$10,$11,$12}' | cut -d':' -f2| cut -d',' -f2 )
        L3=$(egrep "load average" extracted/$i |grep -v top | awk '{print $8,$9,$10,$11,$12}' | cut -d':' -f2| cut -d',' -f3 )
        if [[ $( echo $LOAD_VALUE > $CPU_NUM | bc -l ) = 1 ]]; then
           echo "$i     -       $L1,$L2,$L3 - Avg - $LOAD_VALUE" >> /tmp/VARIABLE_OKAY
        fi
done

[ -s /tmp/VARIABLE_OKAY ] && echo -e "\n7. Load hike found in following servers : \n$(cat /tmp/VARIABLE_OKAY)" || echo -e "\n7. Load is normal in all servers and didn't find any abnormal load spikes."

}

selinux_refine()
{
>/tmp/VARIABLE_OKAY
>/tmp/VARIABLE_NTOKAY
for i in $(cat list); do 
        if [ $(grep "SELinux status"  extracted/$i | grep -c "enabled") -eq 1 ]; then
           echo $i >> /tmp/VARIABLE_OKAY
        fi
        if [ $(grep "SELinux status" extracted/$i | grep -c "disabled") -eq 1 ]; then
           echo $i >> /tmp/VARIABLE_NTOKAY
        fi
done
[ -s /tmp/VARIABLE_NTOKAY ] && echo -e "\n8. SELinux noted as disabled in following servers and KuwaitNET highly recommends configuring SELinux on all servers below to leverage  enterprise security standards to tighten the security of the servers: \n" \
			    && awk '{printf("%02d %s\n", NR, $0)}' /tmp/VARIABLE_NTOKAY | paste -d: - - | column -t -s ":" \
			    || echo -e "\n8. SELinux is enabled in all servers "

#[ -s /tmp/VARIABLE_OKAY ] && echo -e ""$U_L"SELinux enabled in following servers: \n$NC$GREEN$(cat /tmp/VARIABLE_OKAY)$NC"
#[ -s /tmp/VARIABLE_NTOKAY ] && echo -e ""$U_L"SELinux disabled in following servers: \n$NC$RED$(cat /tmp/VARIABLE_NTOKAY)$NC" || echo -e "$GREEN SELinux is enabled in all servers $NC"
}

iptables_refine()
{
>/tmp/VARIABLE_OKAY
>/tmp/VARIABLE_NTOKAY
for i in $(cat list); do

FLAG_IPTABLES=$(sed -n -e '/Firewall Rules/,/Network Errors/p' extracted/$i | egrep -c "REJECT|ACCEPT")
#FLAG_IPTABLES=$(sed -n -e '/Firewall Rules/,/Network Errors/p' extracted/$i | wc -l)
	if [ "$FLAG_IPTABLES" -gt "3" ]; then
           echo $i >> /tmp/VARIABLE_OKAY
        else
           echo $i >> /tmp/VARIABLE_NTOKAY
        fi
done
[ -s /tmp/VARIABLE_NTOKAY ] && echo -e "\n9. IPTABLES not Configured in following servers: \n" \
			    && awk '{printf("%02d %s\n", NR, $0)}' /tmp/VARIABLE_NTOKAY | paste -d: - - | column -t -s ":" \
			    || echo -e "\n9. IPTABLES configured in all servers "

#[ -s /tmp/VARIABLE_OKAY ] && echo -e ""$U_L"IPTABLES Configured in following servers: \n$NC$GREEN$(cat /tmp/VARIABLE_OKAY)$NC"
#[ -s /tmp/VARIABLE_NTOKAY ] && echo -e ""$U_L"IPTABLES not Configured in following servers: \n$NC$RED$(cat /tmp/VARIABLE_NTOKAY)$NC"  || echo -e "$GREEN IPTABLES configured in all servers $NC"
}


uptime_refine()
{
UPTIME_VAR="30"
>/tmp/VARIABLE_OKAY
>/tmp/VARIABLE_NTOKAY
for i in $(cat list); do
	if [ $(grep "Uptime"  extracted/$i | grep -c days) == 1 ]; then
	UP_DAYS=$(grep "Uptime"  extracted/$i | awk '{print $3}')
         	if [ $UP_DAYS -gt $UPTIME_VAR ]; then
           		echo "$i:->:$UP_DAYS Days" >> /tmp/VARIABLE_OKAY
		else 
			echo "$i:->:$UP_DAYS Days" >> /tmp/VARIABLE_NTOKAY
        	fi
	else 
		echo "$i:->:$(grep "Uptime"  extracted/$i | awk '{print $3}')" >> /tmp/VARIABLE_NTOKAY
	fi
		
done

[ -s /tmp/VARIABLE_NTOKAY ] && echo -e "\n4. Uptime of following servers noted as below $UPTIME_VAR days: \n" \
			    && column -t -s ":" /tmp/VARIABLE_NTOKAY \
			    || echo -e "\n4. Uptime of all servers is noted as above $UPTIME_VAR days"

#[ -s /tmp/VARIABLE_NTOKAY ] && echo -e ""$U_L"Uptime of following servers noted as below 60 days: \n$NC$RED$(cat /tmp/VARIABLE_OKAY)$NC" || echo -e "$GREEN Uptime of all servers is noted as above 60 days$NC"
}

package_refine()
{
>/tmp/VARIABLE_PKGLIST
>/tmp/VARIABLE_FINALLIST
for i in $(cat list); do

	sed -n -e '/Redhat Registration Check/,/NTP Service Info/p' extracted/$i | grep -v "NTP Service Info" > /tmp/VARIABLE_PKGLIST

        if [ $(grep -c "Software Updates" /tmp/VARIABLE_PKGLIST) -eq 1 ]; then
           echo -e "Hostname: $i" >> /tmp/VARIABLE_FINALLIST
	   grep "needed for security" /tmp/VARIABLE_PKGLIST >> /tmp/VARIABLE_FINALLIST
	   sed -n -i '/needed for security/,//p' /tmp/VARIABLE_PKGLIST
	   sed -i '1d' /tmp/VARIABLE_PKGLIST
           awk '{print $1"*"$2}' /tmp/VARIABLE_PKGLIST >> /tmp/VARIABLE_FINALLIST
	   echo -e "\n---" >> /tmp/VARIABLE_FINALLIST
        fi
#           sed -i 's/^/    /' /tmp/VARIABLE_FINALLIST
done

[ -s /tmp/VARIABLE_FINALLIST ] && echo -e "\n10. Found following security package updates available in following servers:\n" \
                            && column -t -s "*" /tmp/VARIABLE_FINALLIST  | awk '{print "    "$0}' \
                            || echo -e "\n2. no sw updates in all servers"

}



######################################################################################################################

{
echo -e "\t\t\t==========---  +++  PERIODIC MAITENANCE REPORT  +++  ---==========\n"

#Servers List
echo -e "\t\t\t==========---  ---  Issue details / Debugging  ---  ---==========\n"
servers_list;

#Solution and Details
echo -e "\n\t\t\t==========---  ---  Solution and Details  ---  ---==========\n"
echo -e "Technical Feedback of Health Check Visit: \n"

#RHN Network Check
#echo -e "\t\t\t==========---\t\t    RHN  \t\t  ---==========\n"
rhn_refine;

#Disk
#echo -e "\t\t\t==========---\t\t    Disk Usage  \t\t  ---==========\n"
disk_refine;

#NTP 
#echo -e "\t\t\t==========---\t\t    NTP  \t\t  ---==========\n"
ntp_refine;

#Uptime
#echo -e "\t\t\t==========---\t\t    UPTIME  \t\t  ---==========\n"
uptime_refine;

#Memory
#echo -e "\t\t\t==========---\t\t    Memory Usage  \t\t  ---==========\n"
memory_refine;

#CPU
cpu_refine;

#Load
#echo -e "\t\t\t==========---\t\t    Load   \t\t  ---==========\n"
load_refine;

#SELinux
#echo -e "\t\t\t==========---\t\t    SELINUX  \t\t  ---==========\n"
selinux_refine;

#IPTables
#echo -e "\t\t\t==========---\t\t    IPTABLES  \t\t  ---==========\n"
iptables_refine;

#Package update details
package_refine;

#Logs
echo -e "\n10."
echo -e "\n----------------------------------------------------------------------------------"
echo -e 'Check Logs: \n for i in $(cat list); do echo -e ---------/$i---------;cat extracted/$i | egrep -A100 "Message Log"; done   | more '
echo -e "\n Add below point if error logs is fine: \n *. Analysed all System and Security logs and didn't find any OS related critical failure, warning or error notifications."
} >> "$PM_REPORT"

more "$PM_REPORT"

rm /tmp/VARIABLE_OKAY
rm /tmp/VARIABLE_NTOKAY
rm /tmp/VARIABLE_OKAY_OUT
rm "$PM_REPORT"

exit 0


