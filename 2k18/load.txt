#!/bin/bash
EMERGENCYLOAD=3 #Set this is to the server load during which CPU stats needs to be analysed
CURRENTLOAD=$(uptime | cut -d\, -f3 | cut -d\: -f2 | awk '{printf "%.0f",$1}')
email_id=ansaranz@gmail.com
LOG_FILE=$(date +%d%b%Y)-$(date +"%H-%M-%S").log


load_check () {
	mkdir -p /var/log/loadcheck
	Outputfile="/var/log/loadcheck/$LOG_FILE"
	touch $Outputfile
	> $Outputfile

	echo -e "server: \t\t $(hostname -s)" >> $Outputfile
	echo -e "date:   \t\t $(date +%Y-%m-%d)" >> $Outputfile
	echo -e "time:   \t\t $(date +%T)" >> $Outputfile
	echo -e "host:   \t\t $(hostname -f)" >> $Outputfile
	echo -e "inet:   \t\t $(hostname -i)" >> $Outputfile
	echo -e "Load:   \t\t $CURRENTLOAD" >> $Outputfile
	echo -e "Top Processes: \n $(ps -eo pcpu,pid,user,args | sort -r -k1 | grep -v ^' 0.0')" >> $Outputfile
	echo -e "Memory Usage: \n $(egrep  'Mem|Cache|Swap' /proc/meminfo)" >> $Outputfile


	SUB="Server Load High: $CURRENTLOAD for $(hostname) at $(date +%Y-%m-%d)"
	cat $Outputfile | mail -s "$SUB" $email_id  
	#rm -f $Outputfile
}

  if [ $CURRENTLOAD -gt $EMERGENCYLOAD ]; then
		load_check;
  	else
		echo "load is normal"
#		exit 0
  fi

exit 0

